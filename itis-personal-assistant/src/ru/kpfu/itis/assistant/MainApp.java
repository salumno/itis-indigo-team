package ru.kpfu.itis.assistant;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jfxtras.scene.control.agenda.Agenda;
import ru.kpfu.itis.assistant.model.Appointments;
import ru.kpfu.itis.assistant.model.Subject;
import ru.kpfu.itis.assistant.util.Pomodoro;
import ru.kpfu.itis.assistant.util.SubjectFileAssistant;
import ru.kpfu.itis.assistant.view.*;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private XYChart.Series set = new XYChart.Series<>();

    private File userDataFile = new File("data", "userData.xml");
    private File appointmentsDataFile = new File("data", "appointmentsData.xml");

    private ObservableList<Subject> subjects = FXCollections.observableArrayList();
    private Pomodoro pomodoro = new Pomodoro();
    private Agenda agenda = new Agenda();

    public static void main(String[] args) {
        launch(args);
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public File getAppointmentsDataFile() {
        return appointmentsDataFile;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        updateData();

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Личный помощник ISAPU");

        pomodoroStartCheck();
        initRootLayout();
    }

    private void updateData() {
        SubjectFileAssistant.loadDataFromFile(userDataFile, subjects);

        agenda.appointments().addAll(Appointments.loadData(appointmentsDataFile));
    }

    private void pomodoroStartCheck() {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        boolean pomodoroSelected = prefs.getBoolean("pomodoroSelected", true);
        if (pomodoroSelected) {
            pomodoro.start();
        }
    }

    public void subjectLayoutShow() {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/SubjectLayout.fxml"));
            AnchorPane subjectPane = loader.load();

            rootLayout.setCenter(subjectPane);

            SubjectLayoutController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void settingsLayoutShow() {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/SettingsLayout.fxml"));
            AnchorPane subjectPane = loader.load();

            rootLayout.setCenter(subjectPane);

            SettingsLayoutController controller = loader.getController();
            controller.setPomodoro(pomodoro);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void calendarLayoutShow() {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/CalendarLayout.fxml"));
            BorderPane calendarPane = loader.load();

            calendarPane.setCenter(agenda);

            rootLayout.setCenter(calendarPane);

            CalendarLayoutController controller = loader.getController();
            controller.setMainApp(this);
            controller.updateDateOfStudiesAppointments();
            controller.addSaveHandler();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initRootLayout() {
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/RootLayout.fxml"));
        try {
            rootLayout = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        RootLayoutController controller = loader.getController();
        controller.setMainApp(this);
        controller.setPomodoro(pomodoro);

        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public boolean showSubjectAddDialog(Subject subject) {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/SubjectAddDialog.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setResizable(false);
            dialogStage.setTitle("Новый предмет");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            SubjectAddDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setSubject(subject);

            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showSubjectInfoDialog(Subject selectedSubject) {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/SubjectInfoDialog.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setResizable(false);
            dialogStage.setTitle("Информация о предмете");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            SubjectInfoDialogController controller = loader.getController();
            controller.setStage(dialogStage);
            controller.setSubject(selectedSubject);
            controller.setMainApp(this);

            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void showStatistics() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/Statistics.fxml"));
            BorderPane statistics = loader.load();
            rootLayout.setCenter(statistics);
            StatisticsLayoutController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ObservableList<Subject> getSubjects() {
        return subjects;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public File getUserDataFile() {
        return userDataFile;
    }

    public XYChart.Series getSet() {
        return set;
    }
}
