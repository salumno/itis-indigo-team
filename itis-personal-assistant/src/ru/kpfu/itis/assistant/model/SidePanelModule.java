package ru.kpfu.itis.assistant.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class SidePanelModule {

    private final StringProperty name;

    public SidePanelModule(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

}
