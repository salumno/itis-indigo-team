package ru.kpfu.itis.assistant.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jfxtras.scene.control.agenda.Agenda;
import ru.kpfu.itis.assistant.util.AppointmentFileAssistant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.io.File;
import java.time.LocalDateTime;
import java.time.temporal.Temporal;
import java.util.Calendar;

/**
 * Created by Rustem on 25.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"wholeDay", "summary", "description", "location", "group", "startLocalDateTime", "endLocalDateTime", "homework", "weekly"})
public class Appointments {
    private boolean wholeDay;
    private String summary;
    private String description;
    private String location;
    private Agenda.AppointmentGroupImpl group;
    @XmlTransient
    private Calendar startTime;
    @XmlTransient
    private Calendar endTime;
    @XmlTransient
    private Temporal startTemporal;
    @XmlTransient
    private Temporal endTemporal;
    private String startLocalDateTime;
    private String endLocalDateTime;
    private String homework;
    private boolean weekly;

    public Appointments() {
    }

    public Appointments(LocalDateTime startLocalDateTime, LocalDateTime endLocalDateTime, String description,
                        String location, boolean wholeDay, String summary, Agenda.AppointmentGroupImpl group, String homework,
                        boolean weekly) {
        this.startLocalDateTime = startLocalDateTime.toString();
        if (!wholeDay) this.endLocalDateTime = endLocalDateTime.toString();
        this.homework = homework;
        this.description = description;
        this.location = location;
        this.wholeDay = wholeDay;
        this.summary = summary;
        this.group = group;
        this.weekly = weekly;
    }

    public static void saveData(File file, ObservableList<Agenda.Appointment> agendaAppList) {
        ObservableList<Appointments> appList = FXCollections.observableArrayList();
        for (Agenda.Appointment app : agendaAppList) {
            String homework = "";
            Boolean weekly = false;
            if (app.getDescription().equals("учеба")) {
                homework = ((StudiesAppointment) app).getHomework();
                weekly = ((StudiesAppointment) app).isWeekly();
            }
            appList.add(new Appointments(app.getStartLocalDateTime(), app.getEndLocalDateTime(), app.getDescription(),
                    app.getLocation(), app.isWholeDay(), app.getSummary(),
                    (Agenda.AppointmentGroupImpl) app.getAppointmentGroup(), homework, weekly));
        }
        AppointmentFileAssistant.saveDataToFile(file, appList);
    }

    public static ObservableList<Agenda.AppointmentImplLocal> loadData(File file) {
        ObservableList<Appointments> list = FXCollections.observableArrayList();
        ObservableList<Agenda.AppointmentImplLocal> agendaAppList = FXCollections.observableArrayList();
        AppointmentFileAssistant.loadDataFromFile(file, list);
        for (Appointments app : list) {
            agendaAppList.add(app.getAgendaAppointment());
        }
        return agendaAppList;
    }

    private Agenda.AppointmentImplLocal getAgendaAppointment() {
        Agenda.AppointmentImplLocal agendaApp = new Agenda.AppointmentImplLocal();
        if (description.equals("учеба")) {
            agendaApp = new StudiesAppointment().withHomework(homework).withWeekly(weekly);
        }
        agendaApp.withStartLocalDateTime(LocalDateTime.parse(startLocalDateTime));
        if (!wholeDay) agendaApp.withEndLocalDateTime(LocalDateTime.parse(endLocalDateTime));
        agendaApp.withDescription(description);
        agendaApp.withLocation(location);
        agendaApp.withWholeDay(wholeDay);
        agendaApp.withSummary(summary);
        agendaApp.withAppointmentGroup(group);
        return agendaApp;
    }
}
