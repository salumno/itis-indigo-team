package ru.kpfu.itis.assistant.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import jfxtras.scene.control.agenda.Agenda;

import java.time.LocalDateTime;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class StudiesAppointment extends Agenda.AppointmentImplLocal {
    final private StringProperty homework = new SimpleStringProperty();
    final private BooleanProperty weekly = new SimpleBooleanProperty(false);

    public Boolean isWeekly() {
        return weekly.get();
    }

    public void setWeekly(Boolean weekly) {
        this.weekly.set(weekly);
    }

    public StudiesAppointment withWeekly(Boolean weekly) {
        setWeekly(weekly);
        return this;
    }

    public String getHomework() {
        return homework.get();
    }

    public void setHomework(String homework) {
        this.homework.set(homework);
    }

    public StringProperty homeworkProperty() {
        return homework;
    }

    public StudiesAppointment withHomework(String homework) {
        setHomework(homework);
        return this;
    }

    public Boolean isOver() {
        return LocalDateTime.now().isAfter(getEndLocalDateTime());
    }

    @Override
    public String toString() {
        return super.toString() + ", " + this.getHomework();
    }
}
