package ru.kpfu.itis.assistant.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@XmlRootElement(name = "subjects")
@XmlSeeAlso({Subject.class})
public class SubjectListWrapper {
    private List subjects;

    @XmlElement(name = "subject")
    public List getSubjects() {
        return subjects;
    }

    public void setSubjects(List subjects) {
        this.subjects = subjects;
    }
}
