package ru.kpfu.itis.assistant.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlType;
import java.time.LocalDateTime;


/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
@XmlType(propOrder = {"name", "teacher", "address", "homework", "date"})
public class SubjectInfo {

    protected StringProperty name = new SimpleStringProperty();
    protected StringProperty teacher = new SimpleStringProperty();
    protected StringProperty address = new SimpleStringProperty();
    protected StringProperty homework = new SimpleStringProperty();
    protected StringProperty date = new SimpleStringProperty();

    public String getDate() {
        return date.getValue();
    }

    public void setDate(String date) {
        this.date.setValue(date);
    }

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.parse(date.getValue());
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getTeacher() {
        return teacher.get();
    }

    public void setTeacher(String teacher) {
        this.teacher.set(teacher);
    }

    public StringProperty teacherProperty() {
        return teacher;
    }

    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public StringProperty addressProperty() {
        return address;
    }

    public String getHomework() {
        return homework.get();
    }

    public void setHomework(String homework) {
        this.homework.set(homework);
    }

    public StringProperty homeworkProperty() {
        return homework;
    }
}
