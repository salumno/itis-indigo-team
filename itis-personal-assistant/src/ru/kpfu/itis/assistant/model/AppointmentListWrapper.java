package ru.kpfu.itis.assistant.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

/**
 * Created by Rustem on 26.03.2017.
 */
@XmlRootElement(name = "appointments")
@XmlSeeAlso({Appointments.class})
public class AppointmentListWrapper {
    private List appointments;

    @XmlElement(name = "appointment")
    public List getSubjects() {
        return appointments;
    }

    public void setSubjects(List appointments) {
        this.appointments = appointments;
    }
}