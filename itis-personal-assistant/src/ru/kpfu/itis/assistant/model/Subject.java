package ru.kpfu.itis.assistant.model;

import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.time.LocalDateTime;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"lecture", "practice"})
public class Subject {

    private SubjectInfo lecture = new SubjectInfoLecture();
    private SubjectInfo practice = new SubjectInfoPractice();

    public Subject() {
        this(null);

    }

    public Subject(String name) {
        practice.setName(name);
        practice.setTeacher("");
        practice.setAddress("");
        practice.setHomework("");
        practice.setDate(LocalDateTime.now().toString());

        lecture.setName(name);
        lecture.setTeacher("");
        lecture.setAddress("");
        lecture.setHomework("");
        lecture.setDate(LocalDateTime.now().toString());
    }

    public LocalDateTime getLectureDate() {
        return lecture.getLocalDateTime();
    }

    public void setLectureDate(LocalDateTime date) {
        lecture.setDate(date.toString());
    }

    public LocalDateTime getPracticeDate() {
        return practice.getLocalDateTime();
    }

    public void setPracticeDate(LocalDateTime date) {
        practice.setDate(date.toString());
    }

    public SubjectInfo getLecture() {
        return lecture;
    }

    public void setLecture(SubjectInfo lecture) {
        this.lecture = lecture;
    }

    public SubjectInfo getPractice() {
        return practice;
    }

    public void setPractice(SubjectInfo practice) {
        this.practice = practice;
    }

    public String getName() {
        return practice.getName();
    }

    public void setName(String name) {
        this.practice.setName(name);
        this.lecture.setName(name);
    }

    public StringProperty nameProperty() {
        return practice.nameProperty();
    }

    public String getLectureHomework() {
        return lecture.getHomework();
    }

    public void setLectureHomework(String homework) {
        lecture.setHomework(homework);
    }

    public String getPracticeHomework() {
        return practice.getHomework();
    }

    public void setPracticeHomework(String homework) {
        practice.setHomework(homework);
    }

    public String getLectureTeacher() {
        return lecture.getTeacher();
    }

    public void setLectureTeacher(String lectureTeacher) {
        lecture.setTeacher(lectureTeacher);
    }

    public String getPracticeTeacher() {
        return practice.getTeacher();
    }

    public void setPracticeTeacher(String practiceTeacher) {
        practice.setTeacher(practiceTeacher);
    }

    public String getLectureAddress() {
        return lecture.getAddress();
    }

    public void setLectureAddress(String lectureAddress) {
        lecture.setAddress(lectureAddress);
    }

    public String getPracticeAddress() {
        return practice.getAddress();
    }

    public void setPracticeAddress(String practiceAddress) {
        practice.setAddress(practiceAddress);
    }
}
