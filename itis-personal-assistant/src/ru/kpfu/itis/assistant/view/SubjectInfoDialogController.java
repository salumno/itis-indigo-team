package ru.kpfu.itis.assistant.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import jfxtras.scene.control.LocalDateTimeTextField;
import ru.kpfu.itis.assistant.MainApp;
import ru.kpfu.itis.assistant.model.*;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class SubjectInfoDialogController {

    private final String LECTURE = "Лекция";
    private final String PRACTICE = "Практика";
    private ObservableList<String> types = FXCollections.observableArrayList();
    @FXML
    private TextField name;

    @FXML
    private LocalDateTimeTextField date;

    @FXML
    private ChoiceBox<String> subjectType = new ChoiceBox<>();

    @FXML
    private TextField teacher;

    @FXML
    private TextField address;

    @FXML
    private TextArea homework;

    private Subject subject;
    private SubjectInfo lectureCommit, practiceCommit;
    private Stage stage;
    private MainApp mainApp;
    private boolean okClicked = false;

    @FXML
    private void initialize() {

        choiceBoxSettings();

        subjectType.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showAndSaveSubjectDetails(oldValue, newValue)
        );
    }

    private void commitCurrentInfo(Subject subject) {
        lectureCommit = new SubjectInfoLecture();
        lectureCommit.setName(subject.getName());
        lectureCommit.setTeacher(subject.getLectureTeacher());
        lectureCommit.setHomework(subject.getLectureHomework());
        lectureCommit.setAddress(subject.getLectureAddress());
        lectureCommit.setDate(subject.getLectureDate().toString());

        practiceCommit = new SubjectInfoPractice();
        practiceCommit.setName(subject.getName());
        practiceCommit.setTeacher(subject.getPracticeTeacher());
        practiceCommit.setHomework(subject.getPracticeHomework());
        practiceCommit.setAddress(subject.getPracticeAddress());
        practiceCommit.setDate(subject.getPracticeDate().toString());
    }

    private void showAndSaveSubjectDetails(String oldType, String newType) {
        SubjectInfo oldInfo = null;
        SubjectInfo newInfo = null;

        if (oldType.equals(LECTURE)) {
            oldInfo = subject.getLecture();
        } else if (oldType.equals(PRACTICE)) {
            oldInfo = subject.getPractice();
        }

        if (newType.equals(LECTURE)) {
            newInfo = subject.getLecture();
        } else if (newType.equals(PRACTICE)) {
            newInfo = subject.getPractice();
        }

        if (oldInfo != null) {
            oldInfo.setTeacher(teacher.getText());
            oldInfo.setName(name.getText());
            oldInfo.setAddress(address.getText());
            oldInfo.setHomework(homework.getText());
            oldInfo.setDate(date.getDisplayedLocalDateTime().toString());
        }
        if (newInfo != null) {
            name.setText(newInfo.getName());
            teacher.setText(newInfo.getTeacher());
            address.setText(newInfo.getAddress());
            homework.setText(newInfo.getHomework());
            date.setLocalDateTime(newInfo.getLocalDateTime());
        }
    }

    @FXML
    private void handleOK() {
        if (isInputValid()) {
            SubjectInfo subjectInfo = saveDataToSubject();
            addAppointmentFromSubjectInfo(subjectInfo);
            Appointments.saveData(mainApp.getAppointmentsDataFile(), mainApp.getAgenda().appointments());
            okClicked = true;
            stage.close();
        }
    }

    private void addAppointmentFromSubjectInfo(SubjectInfo currentType) {
        StudiesAppointment studApp = new StudiesAppointment();
        studApp.withStartLocalDateTime(currentType.getLocalDateTime());
        studApp.withEndLocalDateTime(currentType.getLocalDateTime().plusMinutes(90));
        studApp.withSummary(currentType.getName() + "(" + subjectType.getValue() + ")");
        studApp.withDescription("учеба");
        studApp.withLocation(currentType.getAddress());
        studApp.withHomework(currentType.getHomework());
        studApp.withWeekly(true);
        mainApp.getAgenda().appointments().add(studApp);
    }

    private SubjectInfo saveDataToSubject() {
        SubjectInfo currentType = null;
        if (subjectType.getValue().equals(LECTURE)) {
            currentType = subject.getLecture();
        } else if (subjectType.getValue().equals(PRACTICE)) {
            currentType = subject.getPractice();
        }
        if (currentType != null) {
            currentType.setName(name.getText());
            currentType.setTeacher(teacher.getText());
            currentType.setAddress(address.getText());
            currentType.setHomework(homework.getText());
            currentType.setDate(date.getLocalDateTime().toString());
        }
        return currentType;
    }

    @FXML
    private void handleCancel() {
        subject.setLecture(lectureCommit);
        subject.setPractice(practiceCommit);
        stage.close();
    }

    public void setSubject(Subject subject) {
        commitCurrentInfo(subject);
        this.subject = subject;
        name.setText(subject.getName());
        teacher.setText(subject.getLectureTeacher());
        address.setText(subject.getLectureAddress());
        homework.setText(subject.getLectureHomework());

        date.setLocalDateTime(subject.getLectureDate());
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (subject.getName() == null || subject.getName().length() == 0) {
            errorMessage += "Поле \"Название\" обязательно для заполнения!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(stage);
            alert.setTitle("Ошибочка!");
            alert.setHeaderText("К сожалению, что-то заполнено некорректно.");
            alert.setContentText(errorMessage);
            alert.showAndWait();
            return false;
        }
    }

    private void choiceBoxSettings() {
        types.add(LECTURE);
        types.add(PRACTICE);
        subjectType.setItems(types);
        subjectType.setValue(LECTURE);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public boolean isOkClicked() {
        return okClicked;
    }
}
