package ru.kpfu.itis.assistant.view;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ru.kpfu.itis.assistant.MainApp;
import ru.kpfu.itis.assistant.util.Pomodoro;

import java.util.prefs.Preferences;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class RootLayoutController {

    private static final String ON_BUTTON_PATH = "/ru/kpfu/itis/assistant/resources/turned-on-button-icon.png";
    private static final String OFF_BUTTON_PATH = "/ru/kpfu/itis/assistant/resources/turned-off-button-icon.png";
    private Image buttonOnImage = new Image(ON_BUTTON_PATH);
    private Image buttonOffImage = new Image(OFF_BUTTON_PATH);


    private MainApp mainApp;
    private Pomodoro pomodoro;

    @FXML
    private ImageView pomodoroButton = new ImageView();

    @FXML
    private void initialize() {}

    private void pomodoroModeInit() {
        pomodoroButton.setOnMouseClicked(event -> pomodoroButtonHandler());
        if (pomodoro.isWork()) {
            pomodoroButton.setImage(buttonOnImage);
        } else {
            pomodoroButton.setImage(buttonOffImage);
        }
    }

    private void pomodoroButtonHandler() {
        pomodoroSetValue();
        if (pomodoro.isWork()) {
            pomodoroButton.setImage(buttonOnImage);
        } else {
            pomodoroButton.setImage(buttonOffImage);
        }
        savePomodoroData();
    }

    private void pomodoroSetValue() {
        if (!pomodoro.isWork()) {
            pomodoro.start();
        } else {
            pomodoro.stop();
        }
    }

    private void savePomodoroData() {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        prefs.putBoolean("pomodoroSelected", pomodoro.isWork());
    }

    @FXML
    private void handleSubject() {
        mainApp.subjectLayoutShow();
    }

    @FXML
    private void handleCalendar() {
        mainApp.calendarLayoutShow();
    }

    @FXML
    private void handleSettings() {
        mainApp.settingsLayoutShow();
    }

    @FXML
    private void handleStatistics() {
        mainApp.showStatistics();
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setPomodoro(Pomodoro pomodoro) {
        this.pomodoro = pomodoro;
        pomodoroModeInit();
    }
}
