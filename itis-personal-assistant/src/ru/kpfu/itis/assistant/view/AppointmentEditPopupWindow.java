package ru.kpfu.itis.assistant.view;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import jfxtras.scene.control.LocalDateTimeTextField;
import jfxtras.scene.control.agenda.Agenda;
import ru.kpfu.itis.assistant.MainApp;
import ru.kpfu.itis.assistant.model.Appointments;
import ru.kpfu.itis.assistant.model.StudiesAppointment;

import java.time.LocalDateTime;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class AppointmentEditPopupWindow implements Callback<Agenda.Appointment, Void> {

    private Agenda calendar;
    private MainApp mainApp;
    private LocalDateTimeTextField endTextField;

    @Override
    public Void call(Agenda.Appointment appointment) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        Pane pane = new Pane();
        GridPane gridPane = null;
        if (appointment.getDescription().equals("учеба")) {
            window.setTitle("Редактирование учебного мероприятия");
            gridPane = createGridPane(appointment);
            int currentRow = 5;
            gridPane.add(new Label("Заметки"), 0, currentRow);
            gridPane.add(createNodeTextArea(appointment), 1, currentRow);

        } else if (appointment.getDescription().equals("жизнь")) {
            window.setTitle("Редактирование мероприятия");
            gridPane = createGridPane(appointment);
        }
        pane.getChildren().addAll(
                gridPane,
                createButtonOK(window),
                createDeleteButton(window, appointment)
        );
        Scene scene = new Scene(pane, 400, 400);
        window.setScene(scene);
        window.show();
        return null;
    }

    private GridPane createGridPane(Agenda.Appointment appointment) {
        GridPane gridPane = new GridPane();
        gridPane.setLayoutX(10);
        gridPane.setLayoutY(10);

        Label start = new Label("Начало");
        Label stop = new Label("Конец");
        Label summary = new Label("Краткое описание");
        summary.setWrapText(true);
        Label location = new Label("Место");

        gridPane.addColumn(0, start, stop, summary, location, new Label());
        gridPane.addColumn(1,
                createStartTextField(appointment),
                createEndTextField(appointment),
                createSummaryTextField(appointment),
                createLocationTextField(appointment),
                createWholedayCheckbox(appointment)
        );

        return gridPane;
    }

    private Button createButtonOK(Stage window) {
        Button ok = new Button("OK");
        ok.setLayoutX(300);
        ok.setLayoutY(350);
        ok.setPrefSize(90, 10);
        ok.setOnAction((event) -> {
            Appointments.saveData(mainApp.getAppointmentsDataFile(), calendar.appointments());
            window.close();
        });
        return ok;
    }

    private Button createDeleteButton(Stage window, Agenda.Appointment appointment) {
        Button delete = new Button("Удалить");
        delete.setLayoutX(10);
        delete.setLayoutY(350);
        delete.setPrefSize(90, 10);
        delete.setOnAction((event) -> {
            calendar.appointments().remove(appointment);
            Appointments.saveData(mainApp.getAppointmentsDataFile(), calendar.appointments());
            calendar.refresh();
            window.close();
        });
        return delete;
    }

    private LocalDateTimeTextField createStartTextField(Agenda.Appointment appointment) {
        LocalDateTimeTextField startTextField = new LocalDateTimeTextField();
        startTextField.setLocalDateTime(appointment.getStartLocalDateTime());

        // event handling
        startTextField.localDateTimeProperty().addListener((observable, oldValue, newValue) -> {

            // remember
            LocalDateTime lOldStart = appointment.getStartLocalDateTime();

            // set
            appointment.setStartLocalDateTime(newValue);

            // update end date
            if (appointment.getEndLocalDateTime() != null) {

                // enddate = start + duration
                long lDurationInNano = appointment.getEndLocalDateTime().getNano() - lOldStart.getNano();
                LocalDateTime lEndLocalDateTime = appointment.getStartLocalDateTime().plusNanos(lDurationInNano);
                appointment.setEndLocalDateTime(lEndLocalDateTime);

                // update field
                endTextField.setLocalDateTime(appointment.getEndLocalDateTime());
            }

            // refresh is done upon popup close
            calendar.refresh();
        });

        return startTextField;
    }

    private LocalDateTimeTextField createEndTextField(Agenda.Appointment appointment) {
        endTextField = new LocalDateTimeTextField();
        endTextField.setLocalDateTime(appointment.getEndLocalDateTime());
        endTextField.setVisible(appointment.getEndLocalDateTime() != null);

        endTextField.localDateTimeProperty().addListener((observable, oldValue, newValue) -> {
            appointment.setEndLocalDateTime(newValue);
            // refresh is done upon popup close
            calendar.refresh();
        });

        return endTextField;
    }

    private CheckBox createWholedayCheckbox(Agenda.Appointment appointment) {
        CheckBox wholedayCheckBox = new CheckBox("На целый день?");
        wholedayCheckBox.setWrapText(true);
        wholedayCheckBox.setId("wholeday-checkbox");
        wholedayCheckBox.selectedProperty().set(appointment.isWholeDay());

        wholedayCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            appointment.setWholeDay(newValue);
            if (newValue) {
                appointment.setEndLocalDateTime(null);
            } else {
                LocalDateTime lEndTime = appointment.getStartLocalDateTime().plusMinutes(30);
                appointment.setEndLocalDateTime(lEndTime);
                endTextField.setLocalDateTime(appointment.getEndLocalDateTime());
            }
            endTextField.setVisible(appointment.getEndLocalDateTime() != null);
            // refresh is done upon popup close
            calendar.refresh();
        });

        return wholedayCheckBox;
    }

    private TextField createSummaryTextField(Agenda.Appointment appointment) {
        TextField summaryTextField = new TextField();
        summaryTextField.setText(appointment.getSummary());
        summaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            appointment.setSummary(newValue);
            // refresh is done upon popup close
            calendar.refresh();
        });
        return summaryTextField;
    }

    private TextField createLocationTextField(Agenda.Appointment appointment) {
        TextField locationTextField = new TextField();
        locationTextField.setText(appointment.getLocation() == null ? "" : appointment.getLocation());
        locationTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            appointment.setLocation(newValue);
            // refresh is done upon popup close
            calendar.refresh();
        });
        return locationTextField;
    }

    private TextArea createNodeTextArea(Agenda.Appointment appointment) {
        TextArea node = new TextArea();
        node.setWrapText(true);
        node.setPrefSize(200, 200);
        node.setText(((StudiesAppointment) appointment).getHomework() == null ? "" : ((StudiesAppointment) appointment).getHomework());
        node.textProperty().addListener(((observable, oldValue, newValue) -> {
            ((StudiesAppointment) appointment).setHomework(newValue);
            calendar.refresh();
        }));
        return node;
    }

    public void setCalendar() {
        this.calendar = mainApp.getAgenda();
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        setCalendar();
    }
}
