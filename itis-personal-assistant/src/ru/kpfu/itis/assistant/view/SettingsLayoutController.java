package ru.kpfu.itis.assistant.view;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import ru.kpfu.itis.assistant.MainApp;
import ru.kpfu.itis.assistant.util.Pomodoro;

import java.util.prefs.Preferences;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class SettingsLayoutController {

    @FXML
    private CheckBox pomodoroCheckBox = new CheckBox();;

    private Pomodoro pomodoro;

    @FXML
    private void initialize() {}

    public void setPomodoro(Pomodoro pomodoro) {
        this.pomodoro = pomodoro;
        pomodoroCheckBox.setSelected(pomodoro.isWork());
    }

    @FXML
    private void pomodoroCheckBoxHandler() {
        pomodoroSetValue();
        savePomodoroData();
    }

    private void pomodoroSetValue() {
        if (pomodoroCheckBox.isSelected()) {
            pomodoro.start();
        } else {
            pomodoro.stop();
        }
    }

    private void savePomodoroData() {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        prefs.putBoolean("pomodoroSelected", pomodoroCheckBox.isSelected());
    }

}
