package ru.kpfu.itis.assistant.view;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.GridPane;
import jfxtras.scene.control.LocalDateTimeTextField;
import jfxtras.scene.control.agenda.Agenda;
import jfxtras.scene.control.agenda.AgendaSkinSwitcher;
import ru.kpfu.itis.assistant.MainApp;
import ru.kpfu.itis.assistant.model.Appointments;
import ru.kpfu.itis.assistant.model.StudiesAppointment;


/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class CalendarLayoutController {

    private MainApp mainApp;

    @FXML
    private Agenda calendar;

    private ObservableList<Agenda.Appointment> studiesAppointments = FXCollections.observableArrayList();
    private ObservableList<Agenda.Appointment> lifeAppointments = FXCollections.observableArrayList();
    private ObservableList<Agenda.Appointment> currentAppointments = FXCollections.observableArrayList();

    @FXML
    private CheckBox studiesFilter = new CheckBox();
    @FXML
    private CheckBox currentTypeOfAppointment = new CheckBox();

    @FXML
    private GridPane skinSwitcherPanel = new GridPane();
    @FXML
    private GridPane miniCalendarPanel = new GridPane();

    @FXML
    private void initialize() {
    }

    @FXML
    private void studiesFilterHandle() {
        studiesAppointments.clear();
        if (studiesFilter.isSelected()) {
            lifeAppointments.clear();
            collectAppointmentInfoFromCalendar();
            calendar.appointments().setAll(studiesAppointments);
        } else {
            studiesAppointments.clear();
            collectAppointmentInfoFromCalendar();
            currentAppointments.clear();
            currentAppointments.addAll(lifeAppointments);
            currentAppointments.addAll(studiesAppointments);
            calendar.appointments().setAll(currentAppointments);
        }
    }

    public void addSaveHandler() {
        calendar.withAppointmentChangedCallback(param -> {
            Appointments.saveData(mainApp.getAppointmentsDataFile(), calendar.appointments());
            return null;
        });
        calendar.appointments().addListener((ListChangeListener<Agenda.Appointment>)
                c -> Appointments.saveData(mainApp.getAppointmentsDataFile(), calendar.appointments()));
    }

    private void collectAppointmentInfoFromCalendar() {
        //Kinda sort of appointments that on the calendar right now.
        for (Agenda.Appointment currentAppointment : calendar.appointments()) {
            if (currentAppointment.getDescription().equals("учеба")) {
                studiesAppointments.add(currentAppointment);
            } else if (currentAppointment.getDescription().equals("жизнь")) {
                lifeAppointments.add(currentAppointment);
            }
        }
    }

    public void updateDateOfStudiesAppointments() {
        collectAppointmentInfoFromCalendar();
        for (Agenda.Appointment currentAppointment : studiesAppointments) {
            if (((StudiesAppointment) currentAppointment).isWeekly()) {
                while (((StudiesAppointment) currentAppointment).isOver()) {
                    currentAppointment.setStartLocalDateTime(currentAppointment.getStartLocalDateTime().plusDays(7));
                    currentAppointment.setEndLocalDateTime(currentAppointment.getEndLocalDateTime().plusDays(7));
                }
            }
        }
        Appointments.saveData(mainApp.getAppointmentsDataFile(), calendar.appointments());
    }

    @FXML
    private void typeOfAppointmentHandle() {
        //Calendar settings witch connect with appointment's adding.
        calendar.newAppointmentCallbackProperty().set(dateTimeRange -> {
            Agenda.Appointment newAppointment;
            if (currentTypeOfAppointment.isSelected()) {
                newAppointment = new StudiesAppointment()
                        .withHomework("")
                        .withStartLocalDateTime(dateTimeRange.getStartLocalDateTime())
                        .withEndLocalDateTime(dateTimeRange.getEndLocalDateTime())
                        .withSummary("")
                        .withDescription("учеба")
                        .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group2"));
            } else {
                newAppointment = new Agenda.AppointmentImplLocal()
                        .withStartLocalDateTime(dateTimeRange.getStartLocalDateTime())
                        .withEndLocalDateTime(dateTimeRange.getEndLocalDateTime())
                        .withSummary("")
                        .withDescription("жизнь")
                        .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group5"));
            }
            currentAppointments.add(newAppointment);
            return newAppointment;
        });
    }

    public void setCalendar() {
        this.calendar = mainApp.getAgenda();
        //Call method that set start params of nodes. It's not the best place for dat, but it's useful.
        startConfig();
    }

    private void startConfig() {
        //editDialogSetup();
        calendarSetup();

        //Selected skin (day/week) switcher of calendar and add it to the panel.
        AgendaSkinSwitcher skinSwitcher = new AgendaSkinSwitcher(this.calendar);
        skinSwitcherPanel.add(skinSwitcher, 1, 0);

        //Selected text field with current date. Use when we want to choose some concrete date. Add to the panel too.
        LocalDateTimeTextField localDateTimeTextField = new LocalDateTimeTextField();
        localDateTimeTextField.localDateTimeProperty().bindBidirectional(calendar.displayedLocalDateTime());
        miniCalendarPanel.add(localDateTimeTextField, 1, 0);

        //Choose default state for "current type of appointment" checkbox
        currentTypeOfAppointment.setSelected(false);
    }

    private void calendarSetup() {
        //Set default adding of appointments.
        calendar.newAppointmentCallbackProperty().set(dateTimeRange -> new Agenda.AppointmentImplLocal()
                .withStartLocalDateTime(dateTimeRange.getStartLocalDateTime())
                .withEndLocalDateTime(dateTimeRange.getEndLocalDateTime())
                .withSummary("")
                .withDescription("жизнь")
                .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group5")));

        AppointmentEditPopupWindow popupWindow = new AppointmentEditPopupWindow();
        popupWindow.setMainApp(mainApp);
        calendar.setEditAppointmentCallback(popupWindow);
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        setCalendar();
    }
}
