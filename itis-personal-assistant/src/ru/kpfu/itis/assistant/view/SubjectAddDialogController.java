package ru.kpfu.itis.assistant.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ru.kpfu.itis.assistant.model.Subject;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class SubjectAddDialogController {
    @FXML
    private TextField name;
    @FXML
    private TextField lectureTeacher;
    @FXML
    private TextField lectureAddress;
    @FXML
    private TextField practiceTeacher;
    @FXML
    private TextField practiceAddress;

    private Stage dialogStage;
    private Subject subject;
    private boolean okClicked = false;

    @FXML
    private void initialize() {}

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleOK() {
        if (isInputValid()) {
            subject.setName(name.getText());
            subject.setLectureTeacher(lectureTeacher.getText());
            subject.setPracticeTeacher(practiceTeacher.getText());
            subject.setLectureAddress(lectureAddress.getText());
            subject.setPracticeAddress(practiceAddress.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        StringBuilder sb = new StringBuilder();

        if (name.getText() == null || name.getText().length() == 0) {
            sb.append("Название предмета является обязательным для заполнения!\n");
        }

        if ((practiceTeacher.getText() == null || practiceTeacher.getText().length() == 0)
                && (lectureTeacher.getText() == null || lectureTeacher.getText().length() == 0)) {
            sb.append("Введите имя лектора или преподавателя!\n");
        }

        if (sb.length() == 0) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Ого!");
            alert.setHeaderText("Кажется, что-то пошло не так.");
            alert.setContentText(sb.toString());
            alert.showAndWait();

            return false;
        }
    }
}
