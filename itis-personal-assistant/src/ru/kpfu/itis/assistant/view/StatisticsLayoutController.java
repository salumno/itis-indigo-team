package ru.kpfu.itis.assistant.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ru.kpfu.itis.assistant.MainApp;

import java.util.Arrays;


/**
 * Created by Timur on 25.03.2017.
 */

public class StatisticsLayoutController {
    private MainApp mainApp;

    @FXML
    private BarChart sleepChart;

    @FXML
    private CategoryAxis xAxis;

    private ObservableList xes = FXCollections.observableArrayList();
    private ObservableList xes1 = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
        String s = "GOOD";
        String s1 = "EXCELLENT";
        double[] a = {1,2,3,4,5,6,7};
        double[] b = {7,6,5,4,3,2,1};
        setSleepChart(a);
        setWorkChart(b);
        setMoodPicture(s);
        setSubjectUnderstanding(s1);
    }

    private void setWorkChart(double[] a) {
        String[] x = {
                "||||||", "|||||", "||||","|||", "||", "|","сегодня"
        };
        xes1.addAll(Arrays.asList(x));
        y1.setCategories(xes);
        XYChart.Series series = new XYChart.Series<>();
        for(int i = 0; i < a.length; i++){
            series.getData().add(new XYChart.Data<>(x[i],a[i]));
        }
        workChart.getData().add(series);
    }

    private void setSleepChart(double[] a) {
        String[] x = {
                "||||||", "|||||", "||||","|||", "||", "|","сегодня"
        };
        xes.addAll(Arrays.asList(x));
        xAxis.setCategories(xes);
        XYChart.Series series = new XYChart.Series<>();
        for(int i = 0; i < a.length; i++){
            series.getData().add(new XYChart.Data<>(x[i],a[i]));
        }
        sleepChart.getData().add(series);
    }

    private void setMoodPicture(String s) {
        Image p = null;
        switch (s) {
            case "GOOD":
                p = new Image("emoji/GOOD.png");
                break;
            case "PERFECT":
                p = new Image("emoji/PERFECT.png");
                break;
            case "OK":
                p = new Image("emoji/OK.png");
                break;
            case "BAD":
                p = new Image("emoji/BAD.png");
                break;
        }
        moodPicture.setImage(p);
    }

    private void setSubjectUnderstanding(String s) {
        Image p = null;
        switch (s) {
            case "EXCELLENT":
                p = new Image("emoji/EXCELLENT.png");
                break;
            case "NOTBAD":
                p = new Image("emoji/NOTGOOD.png");
                break;
            case "NOTGOOD":
                p = new Image("emoji/NOTBAD.png");
                break;
        }
        subjectUnderstanding.setImage(p);
    }


    @FXML
    private BarChart workChart;

    @FXML
    private CategoryAxis y1;

    @FXML
    private NumberAxis x1;

    @FXML
    private ImageView moodPicture;

    @FXML
    private ImageView subjectUnderstanding;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        sleepChart.getData().addAll(mainApp.getSet());
    }

}

