package ru.kpfu.itis.assistant.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import ru.kpfu.itis.assistant.MainApp;
import ru.kpfu.itis.assistant.model.Subject;
import ru.kpfu.itis.assistant.util.SubjectFileAssistant;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class SubjectLayoutController {

    @FXML
    private TableView<Subject> subjectTable;

    @FXML
    private TableColumn<Subject, String> subjectNameColumn;

    private MainApp mainApp;

    @FXML
    private void initialize() {
        subjectNameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
    }

    @FXML
    private void handleNewSubject() {
        Subject tempSubject = new Subject();
        boolean okClicked = mainApp.showSubjectAddDialog(tempSubject);
        if (okClicked) {
            mainApp.getSubjects().add(tempSubject);
            saveData();
        }
    }

    @FXML
    private void handleOpenSubject() {
        Subject selectedSubject = subjectTable.getSelectionModel().getSelectedItem();
        if (selectedSubject != null) {
            boolean okClicked = mainApp.showSubjectInfoDialog(selectedSubject);
            if (okClicked) {
                saveData();
            }
        } else {
            alertShowWhenNothingSelected();
        }
    }

    @FXML
    private void handleDeleteSubject() {
        int selectedSubject = subjectTable.getSelectionModel().getSelectedIndex();
        Subject currentSubject = subjectTable.getSelectionModel().getSelectedItem();
        if (selectedSubject >= 0) {
            subjectTable.getItems().remove(selectedSubject);
            mainApp.getSubjects().remove(currentSubject);
            saveData();
        } else {
            alertShowWhenNothingSelected();
        }
    }

    private void alertShowWhenNothingSelected() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("Упс!");
        alert.setHeaderText("Вы не выбрали ни одного предмета.");
        alert.setContentText("Пожалуйста, выберите предмет из списка.");
        alert.showAndWait();
    }

    private void saveData() {
        SubjectFileAssistant.saveDataToFile(mainApp.getUserDataFile(), mainApp.getSubjects());
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        subjectTable.setItems(mainApp.getSubjects());
    }
}
