package ru.kpfu.itis.assistant.util;

import javafx.collections.ObservableList;
import ru.kpfu.itis.assistant.model.AppointmentListWrapper;
import ru.kpfu.itis.assistant.model.Appointments;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Rustem on 26.03.2017.
 */
public class AppointmentFileAssistant {

    public static void loadDataFromFile(File file, ObservableList<Appointments> data) {
        try {
            JAXBContext context = JAXBContext.newInstance(AppointmentListWrapper.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            AppointmentListWrapper wrapper = (AppointmentListWrapper) unmarshaller.unmarshal(file);

            data.clear();
            data.addAll(wrapper.getSubjects());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveDataToFile(File file, ObservableList<Appointments> data) {
        try {
            JAXBContext context = JAXBContext.newInstance(AppointmentListWrapper.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            AppointmentListWrapper wrapper = new AppointmentListWrapper();
            wrapper.setSubjects(data);

            file.getParentFile().mkdirs();
            marshaller.marshal(wrapper, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
