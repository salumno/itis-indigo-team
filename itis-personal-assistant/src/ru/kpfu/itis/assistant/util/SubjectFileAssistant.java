package ru.kpfu.itis.assistant.util;

import javafx.collections.ObservableList;
import ru.kpfu.itis.assistant.model.Subject;
import ru.kpfu.itis.assistant.model.SubjectListWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class SubjectFileAssistant {

    public static void loadDataFromFile(File file, ObservableList<Subject> data) {
        try {
            JAXBContext context = JAXBContext.newInstance(SubjectListWrapper.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SubjectListWrapper wrapper = (SubjectListWrapper) unmarshaller.unmarshal(file);

            data.clear();
            data.addAll(wrapper.getSubjects());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveDataToFile(File file, ObservableList<Subject> data) {
        try {
            JAXBContext context = JAXBContext.newInstance(SubjectListWrapper.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            SubjectListWrapper wrapper = new SubjectListWrapper();
            wrapper.setSubjects(data);

            file.getParentFile().mkdirs();
            marshaller.marshal(wrapper, file);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
