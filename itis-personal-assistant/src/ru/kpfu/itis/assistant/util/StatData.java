package ru.kpfu.itis.assistant.util;

/**
 * Created by Timur on 26.03.2017.
 */
public class StatData {
    private int dream;
    private int mood;
    private int burden;
    private int understanding;

    public StatData(int dream, int mood, int burden, int understanding) {
        this.dream = dream;
        this.mood = mood;
        this.burden = burden;
        this.understanding = understanding;
    }

    public void setDream(int dream) {
        this.dream = dream;
    }

    public void setMood(int mood) {
        this.mood = mood;
    }

    public void setBurden(int burden) {
        this.burden = burden;
    }

    public void setUnderstanding(int understanding) {
        this.understanding = understanding;
    }

    public int getDream() {
        return dream;
    }

    public int getMood() {
        return mood;
    }

    public int getBurden() {
        return burden;
    }

    public int getUnderstanding() {
        return understanding;
    }
}
