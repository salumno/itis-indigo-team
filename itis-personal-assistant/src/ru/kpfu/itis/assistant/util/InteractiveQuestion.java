package ru.kpfu.itis.assistant.util;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Timur on 25.03.2017.
 */
public class InteractiveQuestion {

    public static StatData newWindow() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        Pane pane = new Pane();
        window.setTitle("Уделите мне минутку)");

        Button btn = new Button("Передать сведения о статистике");
        btn.setLayoutX(63);
        btn.setLayoutY(140);

        Label l1 = new Label(" 1.Сколько часов ты спал за последние сутки?");
        Label l2 = new Label(" 2.Оцени своё настроение по шкале 2 - 5");
        Label l3 = new Label(" 3.Оцени свою загруженность по шкале 10-100");
        Label l4 = new Label(" 4.Оцени уровень усвоения материала по шкале 1-5");
        l1.setLayoutY(5);
        l2.setLayoutY(40);
        l3.setLayoutY(75);
        l4.setLayoutY(110);

        ChoiceBox cb1 = new ChoiceBox();
        cb1.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
        ChoiceBox cb2 = new ChoiceBox();
        cb2.getItems().addAll(2, 3, 4, 5);
        ChoiceBox cb3 = new ChoiceBox();
        cb3.getItems().addAll(10, 20, 30, 40, 50, 60, 70, 80, 90, 100);
        ChoiceBox cb4 = new ChoiceBox();
        cb4.getItems().addAll(1, 2, 3, 4, 5);

        cb1.setLayoutX(297);
        cb2.setLayoutX(297);
        cb3.setLayoutX(297);
        cb4.setLayoutX(297);
        cb1.setLayoutY(5);
        cb2.setLayoutY(40);
        cb3.setLayoutY(75);
        cb4.setLayoutY(110);
        final StatData[] s = new StatData[1];
        btn.setOnAction(event -> {
            if (cb1.getValue() != null && cb2.getValue() != null && cb3.getValue() != null && cb4.getValue() != null) {
                s[0] = new StatData((int) cb1.getValue(), (int) cb2.getValue(), (int) cb3.getValue(), (int) cb4.getValue());
                window.close();
            }
        });

        pane.getChildren().addAll(btn, l1, l2, l3, l4, cb1, cb2, cb3, cb4);
        Scene scene = new Scene(pane, 350, 170);
        window.setScene(scene);
        window.showAndWait();
        return s[0];
    }
}

