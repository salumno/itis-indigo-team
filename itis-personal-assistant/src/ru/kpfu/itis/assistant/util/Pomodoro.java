package ru.kpfu.itis.assistant.util;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Alert;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 *
 * state = 1 - working state, 25 minutes, 4 times;
 * state = 2 - short rest, 5 minutes;
 * state = 3 - long rest, 15 minutes.
 *
 * Для запуска помидорки достаточно создать экзепляр класса Pomodoro и вызвать метод start().
 * Для остановки помидорки вызываем метод stop().
 * При повторном запуске "помидорный цикл" начинается заново.
 */

public class Pomodoro {

    private final byte WORK = 25;
    private final byte REST = 5;
    private final byte BIG_REST = 15;

    private byte currentTime;
    private byte workCycle;
    private byte state;

    private boolean work;

    private Timeline timer;

    private Alert switchInfo;

    public Pomodoro() {
        work = false;
        workCycle = 0;

        switchInfo = new Alert(Alert.AlertType.INFORMATION);
        alertInitial();

        timer = new Timeline(
                new KeyFrame(
                        Duration.millis(60000),
                        ae -> everyMinAction()
                )
        );

        state = 1;
    }

    private void everyMinAction() {
        currentTime--;
        if (currentTime == 0) {
            timer.stop();
            nextState();
        }
    }

    private void nextState() {
        if (state == 1) {
            if (workCycle == 3) {
                state = 3;
            } else {
                state = (byte) ((workCycle < 4) ? 2 : 3);
            }
        } else if (state == 2) {
            workCycle++;
            state = 1;
        } else if (state == 3) {
            workCycle = 0;
            state = 1;
        }
        showMessage();
        start();
    }

    public void start() {
        work = true;
        switch (state) {
            case 1:
                currentTime = WORK;
                break;
            case 2:
                currentTime = REST;
                break;
            case 3:
                currentTime = BIG_REST;
                break;
        }
        timer.setCycleCount(currentTime);
        timer.play();

    }

    public void stop() {
        work = false;
        timer.stop();
        state = 1;
        workCycle = 0;
    }

    private void alertInitial() {
        switchInfo.setTitle("Помидорка");
        switchInfo.setHeaderText(null);
    }

    private void showMessage() {
        Media sound = new Media(new File("repose.wav").toURI().toString());
        MediaPlayer mp;
        String message;
        if (state == 1) {
            sound = new Media(new File("keepWorking.wav").toURI().toString());
            message = "Вы достаточно отдохнули, принимайтесь за работу!";
        } else if (state == 2) {
            message = "Хватит трудиться, работяга! У вас есть 5 минут на отдых.";
        } else {
            message = "Вы отлично поработали! 15 минут свободного времени вам не повредит.";
        }
        mp = new MediaPlayer(sound);
        mp.play();
        switchInfo.setContentText(message);
        switchInfo.show();
    }

    public boolean isWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }
}

